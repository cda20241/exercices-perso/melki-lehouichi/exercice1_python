def position_caractere(phrase, caractere):
    """
    Cette fonction indique les positions possibles d'un caractère dans une phrase
    :param phrase: (string)
    :param caractere: (string)
    :return: une liste des positions si existe ou liste vide sinon
    """
    position = []
    compteur = 0
    for lettre in phrase :
        compteur += 1
        if lettre == caractere :
            position.append(compteur)
    return position
def ajout_adjectif(position, caractere):
    """
    Cette fonction affiche le texte voulu en lui ajoutant l'adjectif cardinal correspondant
    :param position: (int) la position du caractère dans la phrase
    :param caractere: (string) le  caractère renseigné pour vérifier sa position
    :return: un print() avec ère si pisition au singulier, et avec ème si pluriel
    """
    if int(position) == 1 :
        return print("On trouve le caractère ", caractere, " à la 1ère  position ")
    return print("On trouve le caractère ", caractere, " à la", position,"ème position ")

phrase = input("phrase : ")
caracter = input("caractère : ")
positions = position_caractere(phrase, caracter)
#on vérifie si le caractère existe dans la phrase avant d'affiche ses positions
if caracter in phrase :
    for position in positions :
        ajout_adjectif(position, caracter)
#si le caractère n'existe pas, on affiche un message pour le dire
else :
    print("Désolé ! ce caractère n'est pas dans la phrase.")