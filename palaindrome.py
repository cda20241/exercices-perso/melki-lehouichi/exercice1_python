from unidecode import unidecode

def inverser(text) :
    """
    Cette fonction inverse une chaine de caractère.
    :param text: (string) notre texte
    :return: l'inverse de texte
    """
    inverse = "".join(reversed(text))
    return inverse
def is_palaindrome(text):
    """
    Cette fonction vérifie si un texte est palaindrome. Elle prend un text et en enlève les éspaces et caractères spéciaux ensuite le compare avec son inverse
    en utilisant la fonction inverser().
    :param text: (string) texte
    :return: vrai si c'est palaindrome faux sinon
    """
    symb = ["'", ":", ";", ",", "-", "?", "!", " ", "."]
    for i in text :
        if i in symb :
         text=text.replace(i,"")
    if unidecode(text.lower()) == unidecode(inverser(text).lower()) :
        return True
    return False

onRelance=True
#une boucle qui permet de relancer l'utilisateur afin de savoir s'il veut refaire la partie ou non
while onRelance == True:
    mot = input("veuillez saisir votre mot : ")
    if is_palaindrome(mot):
        print(mot, "est un palaindrome !")
    else :
        print(mot, "n'est pas un palaindrome !")
    reponse = input("On continue ? (o/n) : ")
    onRelance=(reponse in ("o", "O", "oui", "Oui", "OUI"))