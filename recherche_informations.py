LISTE=[
    ("Pascal", "ROSE", 43),
    ("Mickaël", "FLEUR", 29),
    ("Henri", "TULIPE", 35),
    ("Michel", "FRAMBOISE", 35),
    ("Arthur", "PETALE", 35 ),
    ("Michel", "POLLEN", 50),
    ("Maurice", "FRAMBOISE", 42),
]
#1-Les fonctions
def camping(liste):
    """
    Cette fonction prend en paramètre une liste des personnes et retourne uniquement celles qui adorent le camping selon
    les conditions prédéfinies (prénom : Michel et age égale ou supérieur à 50)
    :param liste: (lise) la liste des personnes à deux démensions
    :return: (liste) liste des personnes qui adorent le camping
    """
    aime_camping=[]
    for element in liste :
        if element[0] == "Michel" or element[2]>=50 :
            aime_camping.append(element)
    return aime_camping

def nombre_lettre(liste):
    """
    Cette fonction vérifie si des personne ont prénom et nom avec même nbr de lettres
    :param liste: lise des personnes à deux démensions
    :return: liste des personnes recherchées et le nbr de lettre de leurs prénoms et noms
    """
    personne=[]
    nbr_lettre=[]
    for pers in liste :
        if len(pers[0]) == len(pers[1]):
            personne.append(pers)
            nbr_lettre.append(len(pers[0]))
    return [personne,nbr_lettre]
#Pour les prénoms identiques
def prenoms_identiques(liste):
    """
    Cette fonction retourne les personnes ayant des prénoms identiques
    :param liste: lise des personnes à deux démenons
    :return: liste des personnes recherchées
    """
    doublons = []
    les_prenoms = []
    for prenom in liste:
        les_prenoms.append(prenom[0])
    for personne in liste:
        if les_prenoms.count(personne[0]) > 1:
            doublons.append(personne)
    return doublons

#Pour les noms identiques :
def noms_identiques(liste):
    """
    Cette fonction retourne les personnes ayant des noms identiques
    :param liste: lise des personnes à deux démentions
    :return: liste des personnes recherchées
    """
    doublons_nom = []
    noms=[]
    for nom in liste :
        noms.append(nom[1])
    for personne in liste:
        if noms.count(personne[1]) > 1:
            doublons_nom.append(personne)
    return doublons_nom

#Les personnes ayant le même age
def meme_age(liste):
    """
    Cette fonction retourne les personnes ayant des âges identiques
    :param liste: lise des personnes à deux démentions
    :return: liste des personnes recherchées
    """
    sans_double_age = []
    ages=[]
    for age in liste :
        ages.append(age[2])
    for personne in liste:
        if ages.count(personne[2]) > 1:
            sans_double_age.append(personne)
    return sans_double_age
#2- exécution du code
#partie 1 & 2 : vérifier si une ou plusieurs personnes adore le campagne ensuite afficher leurs informations
personne_adorant_camping=camping(LISTE)
if personne_adorant_camping != None:
    print("On a dans la liste quelqu'un qui adore le camping !")
    for i in range(0, len(personne_adorant_camping)) :
        print("C'est formidable ",personne_adorant_camping[i], " adore le camping !")
else:
    print("il n'y a personne qui aime le camping !")
nom_prenom=nombre_lettre(LISTE)[0]
nblettre=nombre_lettre(LISTE)[1]
if nom_prenom != None and nblettre != None :
    for i in range(0, len(nom_prenom)):
        print("C'est formidable ",nom_prenom[i], " à ", nblettre[i], " lettres dans son nom et son prénom ! ")

print(prenoms_identiques(LISTE), " ont des prénoms identiques")
print(noms_identiques(LISTE), " ont des noms identiques")
print(meme_age(LISTE), " ont le même âge")