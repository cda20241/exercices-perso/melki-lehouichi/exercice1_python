def matrice(nbLignes, nbColonnes) :
    """Cette fonction prend en paramètre un nombre de lignes et nombre de colonnes pour créer une matrice nulle"""
    uneMatrice=[]
    for noLigne in range(nbLignes):
        uneLigne = [0] * nbColonnes
        uneMatrice += [uneLigne]
    return uneMatrice

def change_valeur(noLigne, noColonne, nouvelleValeur, uneMatrice):
    """Cette fonction prend en valeur une matrice, un numéro de ligne, un numéro de colonne et une valeur elle retourne la matrice
    après avoir changé la valeur donnée dans les coordonnées données"""
    uneMatrice[noLigne][noColonne] = nouvelleValeur
    return uneMatrice

def matrice_identite(nbLignes, nbColonnes) :
    """Cette fonction prend en paramètre nombre de lignes et de colonnes et retourne une matrice identité"""
    matID = matrice(nbLignes,nbColonnes)
    for noLignes in range(nbLignes):
        for noColonnes in range(nbColonnes):
            if noColonnes == noLignes:
                matID[noLignes][noColonnes] += 1
    return matID

def print_matrice(uneMatrice) :
    """Cette fonction prend en paramètre une matrice et l'affiche"""
    for i in uneMatrice:
            print(i)

def dimension_check(matrice1, matrice2):
    """Cette fonction prend en paramètre 2 matrices et qui retourne True si elles sont de même dimension, False sinon"""
    nbLignes1 = len(matrice1)
    nbColonnes1 = len(matrice1[0])
    nbLignes2 = len(matrice2)
    nbColonnes2 = len(matrice2[0])
    if nbLignes1 == nbLignes2 and nbColonnes1 == nbColonnes2:
        return True
    return False

def somme_matrice(matrice1, matrice2):
    """prend en paramètre 2 matrices et en retourne la somme"""
    if  dimension_check(matrice1, matrice2) == False:
         raise TypeError("MatriceDimensionError()")
    nbLignes = len(matrice1)
    nbColonnes = len(matrice1[0])
    matriceSomme = matrice(nbLignes, nbColonnes)
    for noLigne in range(nbLignes):
        for noColonne in range(nbColonnes):
            matriceSomme[noLigne][noColonne] = matrice1[noLigne][noColonne] + matrice2[noLigne][noColonne]
    return matriceSomme

def is_element(nombre, uneMatrice):
    """Cette fonction prend en paramètre une matrice et un nombre et vérifie si ce nombre est dans la matrice ensuite retourne sa position x, y"""
    nbLignes = len(uneMatrice)
    nbColonnes = len(uneMatrice)
    for noLigne in range(nbLignes):
        for noColonne in range(nbColonnes):
            if nombre == uneMatrice[noLigne][noColonne]:
                coordonnées = [noLigne, noColonne]
                return coordonnées

def produit_scalaire(k, uneMatrice) :
    """Cette fonction prend en paramètre une matrice et un nombre entier ou décimal, et en retourne le produit scalaire"""
    nbLignes = len(uneMatrice)
    nbColonnes = len(uneMatrice[0])
    matriceMult = matrice(nbLignes, nbColonnes)
    for noLigne in range(nbLignes):
        for noColonne in range(nbColonnes):
            matriceMult[noLigne][noColonne] = k*uneMatrice[noLigne][noColonne]
    return matriceMult

def multiplie(matrice1, matrice2) :
    """Cette fonction prend en paramètre 2 matrices, et en retourne le produit matriciel"""
    nbLignes1 = len(matrice1)
    nbColonnes1 = len(matrice1[0])
    nbLignes2 = len(matrice2)
    nbColonnes2 = len(matrice2[0])
    if nbColonnes1 != nbLignes2 :
        raise TypeError("MatriceDimensionError()")
    matriceProduit = matrice(nbLignes1, nbColonnes2)
    for numéroLigne in range(nbLignes1) :
        for numéroColonne in range(nbColonnes2) :
            s = 0
            for index in range(nbColonnes1) :
                s = s + matrice1[numéroLigne][index] * matrice2[index][numéroColonne]
            matriceProduit[numéroLigne][numéroColonne] = s
    return matriceProduit