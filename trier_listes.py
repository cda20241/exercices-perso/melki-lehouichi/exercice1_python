import random
LISTE=[
   ["Pascal", "ROSE", 43],
   ["Mickaël", "FLEUR", 29],
   ["Henri", "TULIPE", 35],
   ["Michel", "FRAMBOISE", 35],
   ["Arthur", "PETALE", 35 ],
   ["Michel", "POLLEN", 50],
   ["Maurice", "FRAMBOISE", 42],
    ["Marion", "COTILLA", 42],
    ["Frédéric", "LECLERC", 42],
    ["Jean-Claude", "VANDAM", 42],
]
PRENOM=0
NOM=1
AGE=2
def tri_par_element(liste_personnes, a):
    """
    Cette fonction prend en paramètre une liste de personnes et un index et retourne une copie de la liste
    triée dans l'ordre par l'élément indexé
    """
    liste=liste_personnes.copy()
    listeP=[]
    listeG=[]
    if len(liste) >= 2 :
        x = liste.pop(len(liste)//2)
        for e in range(0, len(liste)) :
            if liste[e][a] <= x[a]:
                listeP.append(liste[e])
            else :
                listeG.append(liste[e])

        return tri_par_element(listeP, a) + [x] + tri_par_element(listeG, a)
    else :
        return liste
def tri_double_par_nom_prenom(liste_personnes):
    """
    Cette fonction prend en paramètre une liste de personnes et retourne une copie de la liste
    triée dans l'ordre par nom ensuite par prénom
    """
    liste=liste_personnes.copy()
    listeP=[]
    listeG=[]
    if len(liste) >= 2 :
        x = liste.pop(len(liste)//2)
        for e in range(0, len(liste)) :
            if liste[e][1] < x[1] or (liste[e][1] == x[1] and liste[e][0] < x[0]):
                listeP.append(liste[e])
            else :
                listeG.append(liste[e])

        return tri_double_par_nom_prenom(listeP) + [x] + tri_double_par_nom_prenom(listeG)
    else :
        return liste

def tri_inverse_par_element(liste_personnes,a):
    """
    Cette fonction prend en paramètre une liste de personnes et un index et retourne une copie de la liste
    triée dans l'ordre inverse par l'élément indexé
    """
    liste=liste_personnes.copy()
    listeP=[]
    listeG=[]
    if len(liste) >= 2 :
        x = liste.pop(0)
        for e in range(0, len(liste)) :
            if liste[e][a] >= x[a] :
                listeG.append(liste[e])
            else :
                listeP.append(liste[e])

        return tri_inverse_par_element(listeG,a) + [x] + tri_inverse_par_element(listeP,a)
    else :
        return liste

#exécution du code
#d'abord on mélange l'ordre de la liste
random.shuffle(LISTE)
# Liste 1 : tri par ordre alphabétique des noms
print("## Liste 1 : triée par ordre alphabétique des noms ##")
for e in tri_par_element(LISTE, NOM) :
    print(e[0],  e[1], e[2], " ans")
# Liste 2 : tri par ordre alphabétique des prénoms
print("## Liste 2 : triée triée par ordre alphabétique inversé des prénoms ##")
for e in tri_inverse_par_element(LISTE, PRENOM) :
    print(e[0],  e[1], e[2], " ans")


# print("trier par prénom \n",tri_par_element(LISTE,0), "\n")
# print( "tri inverse par prénom \n",tri_inverse_par_element(LISTE, 0), "\n")
# print("trier par nom \n",tri_par_element(LISTE,1), "\n")
# print( "tri inverse par nomp \n",tri_inverse_par_element(LISTE, 1))
# print("trier par age \n",tri_par_element(LISTE,2), "\n")
# print( "tri inverse par age \n",tri_inverse_par_element(LISTE, 2))
print("tri double nom et prénom \n", tri_double_par_nom_prenom(LISTE), "\n")







