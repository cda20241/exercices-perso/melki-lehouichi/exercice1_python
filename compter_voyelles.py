#Exercice 1 d'algorithmie en python
def compteur_voyelles(phrase):
    """
    cette fonction prend en paramètre une chaine de caractères et en compte le nombre de voyelles ensuite elle retourne le nombre de chaque voyelle dans un dictionnaire
    :param phrase: (string) : notre phrase
    :return: nombre total de voyelles et le nombre d'occurences de chaque voyelle
    """
    dic = {}
    minuscules = phrase.lower()
    voyelles="aeiouyàâäéèêëîïôöùûüÿ"
    for voyelle in voyelles:
        nbvoyelles = 0
        for lettre in minuscules :
            if lettre == voyelle :
                # si la lettre est une voyelle, on incrémente alors le compteur et on l'ajoute au dictionnaire
                nbvoyelles += 1
                dic[lettre] = nbvoyelles
    return dic

onRelance=True
#une boucle qui permet de relancer l'utilisateur afin de savoir s'il veut refaire la partie ou non
while onRelance == True:
    phrase = input("Vueillez saisir votre phrase : ")
    #on stocke le nombre total des voyelles ainsi que le nombre d'occurences à l'aide des fonction sum() et compteur_voyelle()
    total_voyelles = sum(compteur_voyelles(phrase).values())
    nb_ocuurences_voyelle = compteur_voyelles(phrase)
    print("Le nombre total de voyelles dans votre phrase est : ",total_voyelles)
    print("dont :")
    for cle, valeur in nb_ocuurences_voyelle.items():
        print("Le nombre de ", cle, " est " , valeur)
    reponse = input("On continue ? (o/n) : ")
    onRelance=(reponse in ("o", "O", "oui", "Oui", "OUI"))